all: interp
%.o: %.mas
	marie-as < $< -o $@

LD := marie-ld

%: %.o
	$(LD) -o $@ $^

run: interp lib.fs
	cat $(word 2,$^) /dev/stdin | marie-$@ $<
